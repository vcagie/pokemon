import styled from '@emotion/styled';


const PokemonDetailWrapper = styled.div`
  .homeButton {
    background-color: #f8f8ff;
    width: 90%;
    display: block;
    border-radius: 5px;
    text-align: center;
    padding: 4px 0;
    margin: 2% auto;
    cursor: pointer;
    transition: 0.5s ease;
    transition: transform 0.5s ease;

    :hover {
      transform: scale(1.03);
      box-shadow: 0 2px 2px 0 rgba(0, 0, 0, 0.2), 0 2px 2px 0 rgba(0, 0, 0, 0.2);
    }
  }
`;

export { PokemonDetailWrapper };