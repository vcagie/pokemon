import React from 'react';
import { useQuery } from "@apollo/client";
import { GET_POKEMONS_DETAIL } from "../../graphql/GetPokemonDetail";
import {useParams} from 'react-router-dom';
import PokemonDetailData from '../../components/PokemonDetail/PokemonDetailData';
import { PokemonDetailWrapper } from './PokemonDetail.style';
import LoadingPage from '../../components/LoadingPage/LoadingPage';
import ErrorPage from '../../components/ErrorPage/errorPage';

const PokemonDetail = () =>{
    let {pokemonName} = useParams();

    const gqlVariables = {
        name: pokemonName,
    };

    const { loading, error, data: { pokemon = {} } = {} } = useQuery(
      GET_POKEMONS_DETAIL,
      {
        variables: gqlVariables,
      }
    );
        return loading ? <LoadingPage/> 
        : error ? <ErrorPage/>
        :(
          <PokemonDetailWrapper>
            <PokemonDetailData detail={pokemon}></PokemonDetailData>
          </PokemonDetailWrapper>
        );
}

export default PokemonDetail;