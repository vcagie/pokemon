import React, { useEffect, useState } from "react";
import MyPokemonCard from "../../components/MyPokemon/MyPokemonCard";
import { MyPokemonListWrapper, EmptyPageWrapper } from "./MyPokemonList.style";

const MyPokemonList = () => {
  const [display, setDisplay] = useState("none");
  const [nickname, setNickname] = useState("");
  const [name, setName] = useState("");
  const [catchPoke, setCatchPoke] = useState(
    JSON.parse(localStorage.getItem("MyPokemonList")) || []
  );

  const getPokemonInfo = (nickname, name) => {
    setNickname(nickname);
    setName(name);
  };

  const releasePokemon = () => {
    setCatchPoke((poke) =>
      poke.filter((Q) => !(Q.name === name && Q.nickname === nickname))
    );

    closeModal();
  };

  useEffect(() => {
    localStorage.removeItem("MyPokemonList");
    localStorage.setItem("MyPokemonList", JSON.stringify(catchPoke));
  }, [catchPoke]);

  const openModal = (show) => {
    setDisplay(show);
  };

  const closeModal1 = (event) => {
    if (event.target === document.getElementById("myModal")) {
      closeModal();
    }
  };

  const closeModal = () => {
    setDisplay("none");
  };

  return catchPoke.length === 0 ? (
    <EmptyPageWrapper>
      <div className="textContainer">You Haven't Caught any Pokemon.</div>
    </EmptyPageWrapper>
  ) : (
    <MyPokemonListWrapper display={display}>
      <div id="myModal" className="modal" onClick={closeModal1}>
        <div className="modalContent">
          <div className="modalBody">
            <div className="title">Are you sure?</div>
            <div className="subTitle">Do you want to release {nickname}?</div>
            <div className="confirmationContainer">
              <div className="cancelButton" onClick={closeModal}>
                Cancel
              </div>
              <div className="releaseButton" onClick={releasePokemon}>
                Release
              </div>
            </div>
          </div>
        </div>
      </div>

      <div className="cardList">
        {catchPoke.map((pokemonData, i) => (
          <MyPokemonCard
            data={pokemonData}
            key={i}
            getPokemonInfo={getPokemonInfo}
            showModal={openModal}
          ></MyPokemonCard>
        ))}
        <div className="blankContainer"></div>
        <div className="blankContainer"></div>
        <div className="blankContainer"></div>
        <div className="blankContainer"></div>
        <div className="blankContainer"></div>
      </div>
    </MyPokemonListWrapper>
  );
};

export default MyPokemonList;
