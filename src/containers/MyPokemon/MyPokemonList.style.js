import styled from "@emotion/styled";

const MyPokemonListWrapper = styled.div`
  .cardList {
    display: flex;
    flex-flow: row wrap;
    justify-content: space-between;
    margin: 0% 3%;
  }

  .blankContainer{
    width: 15%;
  }

  .modal {
    display: ${(props) => props.display};
    position: fixed;
    z-index: 1;
    left: 0;
    top: 0;
    padding-top: 200px;
    width: 100%;
    height: 100%;
    overflow: auto;
    background-color: rgba(0, 0, 0, 0.4);

    .modalContent {
      position: relative;
      background-color: white;
      margin: auto;
      padding: 0;
      width: 40%;
      box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2),
        0 6px 20px 0 rgba(0, 0, 0, 0.19);
      -webkit-animation-name: animatetop;
      -webkit-animation-duration: 0.4s;
      animation-name: animatetop;
      animation-duration: 0.4s;
      border-radius: 15px;
    }

    .modalBody {
      padding: 5% 5%;;
      text-align: left;

      .title{
        font-size: 2.5vw;
        font-weight: 800;
        padding: 1% 0;
      }

      .subTitle{
        font-size: 1.5vw;
        padding: 1% 0 3% 0;
      }

      .confirmationContainer {
        display: flex;
        justify-content: space-between;

        .releaseButton, .cancelButton {
          width: 48%;
          text-align: center;
          padding: 10px 0;
          border: 1px solid #F86027;
          font-size: 1.5vw;
          font-weight: 800;
          border-radius: 10px;
          cursor: pointer;
          transition: 0.5s;

          :hover{
            transform: scale(1.03);
          }
        }

        .releaseButton{
          background-color: #F86027;
          color: white;
        }

        .cancelButton {
          color: #F86027;
        }
      }
    }
  }

  @-webkit-keyframes animatetop {
    from {
      top: -300px;
      opacity: 0;
    }
    to {
      top: 0;
      opacity: 1;
    }
  }

  @keyframes animatetop {
    from {
      top: -300px;
      opacity: 0;
    }
    to {
      top: 0;
      opacity: 1;
    }
  }

  @media only screen and (max-width: 480px) {
    .cardList {
      margin: 0% 10%;
    }
  
    .modal {
      padding-top: 250px;
  
      .modalContent {
        width: 80%;
      }
  
      .modalBody {
        .title{
          font-size: 6vw;
          padding: 2% 0;
        }
  
        .subTitle{
          font-size: 3.5vw;
          padding: 1% 0 8% 0;
        }
  
        .confirmationContainer {
        justify-content: space-between;
        .releaseButton, .cancelButton {
            font-size: 3vw;
          }
        }
      }
    }
  }
`;


const EmptyPageWrapper = styled.div`
  width:100%;
  
  .textContainer{
    width:50%;
    margin:4% auto;
    text-align: center;
    background-image: linear-gradient(135deg, #ffdf91, #eaac7f);
    border-radius:20px;
    font-size: 3vw;
    font-weight:900;
    color:white;
    padding: 2% 0;
  }

  @media only screen and (max-width: 480px) {
    .textContainer{
      width:70%;
      margin:10% auto;
      font-size: 5vw;
      padding: 3% 0;
    }
  }
`;

export { MyPokemonListWrapper, EmptyPageWrapper };
