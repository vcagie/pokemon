import React, { useState } from 'react'
import { useQuery } from "@apollo/client";
import { GET_POKEMONS } from "../../graphql/GetPokemonList";
import PokemonData from '../../components/PokemonList/PokemonData';
import { PokemonListWrapper } from './PokemonList.style';
import LoadingPage from '../../components/LoadingPage/LoadingPage';
import ErrorPage from '../../components/ErrorPage/errorPage';

const gqlVariables = {
    limit: 54,
    offset: 1,
};

const PokemonList = () =>{
  const { loading, error, data: { pokemons = [] } = {} } = useQuery(
    GET_POKEMONS,
    {
      variables: gqlVariables,
    }
  );
  const [catchPoke] = useState(
    JSON.parse(localStorage.getItem('MyPokemonList')) || []
  );

  return loading ? <LoadingPage/> 
  : error ? <ErrorPage/>
  :(
    <PokemonListWrapper>
      <div className="cardList">
        {pokemons.results.map((pokemon, i) => {
          return (
            <PokemonData
              pokemon={pokemon}
              key={i}
              ownedPokemon={catchPoke}
            ></PokemonData>
          );
        })}
        <div className="blankContainer"></div>
        <div className="blankContainer"></div>
        <div className="blankContainer"></div>
        <div className="blankContainer"></div>
        <div className="blankContainer"></div>
      </div>
    </PokemonListWrapper>
  );
}

export default PokemonList;