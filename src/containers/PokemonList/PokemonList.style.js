import styled from '@emotion/styled';

const PokemonListWrapper = styled.div`
  .cardList {
    display: flex;
    flex-flow: row wrap;
    justify-content: space-between;
    margin: 0% 3%;
  }

  .blankContainer{
    width: 15%;
  }

  @media only screen and (max-width: 480px) {
    .cardList {
      margin: 0% 10%;
    }
  }
`;

export {PokemonListWrapper};