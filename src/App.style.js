import styled from "@emotion/styled";

const AppWrapper = styled.div`
  padding-bottom: 1%;

  .background {
    position: fixed;
    top: -50%;
    left: -50%;
    width: 200%;
    height: 200%;
    z-index: -1;

    .backgroundImg {
      position: absolute;
      margin: auto;
      top: 0;
      left: 0;
      right: 0;
      bottom: 0;
      object-fit: cover;
      min-height: 50%;
      min-width: 50%;
      opacity: 0.5;
    }
  }

  .logoFrame {
    display: block;
    width: 15%;
    padding: 2% 0 2% 3%;
  }

  .content {
    .topBar {
      display: flex;
      padding: 0 0 2% 3%;

      .leftBar,
      .rightBar {
        width: 12%;
        text-align: center;

        a{
          padding-bottom: 7px;
          display: block;
          border-bottom: 1px solid #E0E0E0;
          font-weight:700;
          text-decoration: none;
          color: black;
        }

        a:hover,
        .active {
          color: #005baa;
          border-bottom: 2px solid #005baa;
        }
      }
    }
  }

  @media only screen and (max-width: 480px){
  padding-bottom: 10%;
  .logoFrame {
      width: 40%;
      padding: 8% 0;
      margin: auto;
    }

    .content {
      .topBar {
        padding-bottom: 5%;
        .leftBar,
        .rightBar {
          width: 50%;
        }
      }
    }
  }

  @media only screen and (min-width: 481px) and (max-width: 768px) {
    .content {
      .topBar {
        .leftBar,
        .rightBar {
          width: 20%;
          a{
            font-size: 2vw;
          }
        }
      }
    }
  }

  @media only screen and (min-width: 769px) and (max-width: 1024px){
    .content {
      .topBar {
        .leftBar,
        .rightBar {
          width: 20%;
          a{
            font-size: 2vw;
          }
        }
      }
    }
  }
`;

export { AppWrapper };
