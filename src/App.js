import React from 'react';
import PokemonList from './containers/PokemonList/PokemonList';
import NotFound from './containers/NotFound/NotFound';
import PokemonDetail from './containers/PokemonDetail/PokemonDetail';
import MyPokemonList from './containers/MyPokemon/MyPokemonList';
import { ApolloProvider, ApolloClient, InMemoryCache } from "@apollo/client";
import { BrowserRouter, NavLink, Route, Switch } from 'react-router-dom';
import './App.css';
import PokemonLogo from './images/pokemon-logo1.png';
import { AppWrapper } from './App.style';
import background from './images/background.jpg';

function App() {
  const client = new ApolloClient({
    uri: 'https://graphql-pokeapi.vercel.app/api/graphql',
    cache: new InMemoryCache()
  });

  return (
    <ApolloProvider client={client}>
      <AppWrapper>
        <div className="background">
          <img className="backgroundImg" src={background} alt=""></img>
        </div>
        <img className="logoFrame" src={PokemonLogo} alt=""></img>

        <BrowserRouter>
          <div className="content">
            <div className="topBar">
              <div className="leftBar">
                <NavLink exact to="/" activeClassName="active" >
                  Pokemon List
              </NavLink>
              </div>
              <div className="rightBar">
                <NavLink exact to="/my-pokemon" activeClassName="active" >
                  My Pokemon
              </NavLink>
              </div>
            </div>

            <Switch>
              <Route path="/" component={PokemonList} exact />
              <Route path="/my-pokemon" component={MyPokemonList} exact />
              <Route path="/:pokemonName" component={PokemonDetail} exact />
              <Route component={NotFound} />
            </Switch>
          </div>
        </BrowserRouter>
      </AppWrapper>
    </ApolloProvider>
  );
}

export default App;
