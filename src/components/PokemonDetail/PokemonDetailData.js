import React, { useEffect, useState } from "react";
import { PokemonDetailDataWrapper } from "./PokemonDetailData.styled";

const PokemonDetailData = ({ detail }) => {
  const [catchPoke, setCatchPoke] = useState(
    JSON.parse(localStorage.getItem("MyPokemonList")) || []
  );
  const [successCatch, setSuccessCatch] = useState(0);
  const [nickname, setNickname] = useState("");
  const [display, setDisplay] = useState("none");
  const [successDisplay, setSuccessDisplay] = useState("none");
  const [errorMessage, setErrorMessage] = useState("");

  let modal;

  const getPokemon = () => {
    let rate = Math.random();
    if (rate < 0.5) {
      setSuccessCatch(1);
    } else {
      setSuccessCatch(2);
    }
    setDisplay("block");
  };

  const catchNewPokemon = (pokemon) => {
    if (
      catchPoke.some((Q) => Q.name === pokemon.name && Q.nickname === nickname)
    ) {
      setErrorMessage("Nickname telah digunakan");
      setNickname("");
      return;
    } else if (nickname.length === 0) {
      setErrorMessage("Nickname harus diisi");
      setNickname("");
      return;
    } else if (nickname.length > 15){
      setErrorMessage("Nickname harus kurang dari 16 karakter");
      setNickname("");
      return;
    }

    let data = Object.assign({}, pokemon);
    data.nickname = nickname;

    setCatchPoke((poke) => {
      poke = [...poke, data];
      poke.sort(function (a, b) {
        return a.name - b.name;
      });

      return poke;
    });

    setSuccessCatch(0);
    setErrorMessage("");
    setNickname("");
    setDisplay("none");
    setSuccessDisplay("block");
  };

  const ChangeNicknameHandler = (event) => {
    event.persist();
    setNickname(event.target.value);
  };

  useEffect(() => {
    localStorage.setItem("MyPokemonList", JSON.stringify(catchPoke));
  }, [catchPoke]);

  const closeModal1 = (event) => {
    if (event.target === document.getElementById("myModal")) {
      closeModal();
    }
  };

  const closeModal = () => {
    setDisplay("none");
    setErrorMessage("");
    setNickname("");
  };

  const closeSuccessModal1 = (event) => {
    if (event.target === document.getElementById("mySuccessModal")) {
      closeSuccessModal();
    }
  };

  const closeSuccessModal = () =>{
    setSuccessDisplay("none");
  }

  if (successCatch === 1) {
    modal = (
      <div id="myModal" className="modal" onClick={closeModal1}>
        <div className="modalContent">
          <div className="modalBody">
            <div className="title">Yay, You Catch {detail.name}</div>
            <div className="subTitle">Give your new pokemon a nickname!</div>
            <div className="modalBodyInput">
              <div>Nickname</div>
              <input
                type="text"
                value={nickname}
                onChange={ChangeNicknameHandler}
                name="nickname"
                className="nicknameBox"
              ></input>
            </div>
            <div className="errorMessage">{errorMessage}</div>

            <div
              className="submitButton"
              onClick={() => catchNewPokemon(detail)}
            >
              Submit
            </div>
          </div>
        </div>
      </div>
    );
  } else if (successCatch === 2) {
    modal = (
      <div id="myModal" className="modal" onClick={closeModal1}>
        <div className="modalContent">
          <div className="modalBody">
            <div className="title">Failed to Catch {detail.name}</div>
            <div className="subTitle">Please try again</div>
            <div className="tryAgainButton" onClick={closeModal}>
              Try Again
            </div>
          </div>
        </div>
      </div>
    );
  }

  let successNoty = (
    <div
      id="mySuccessModal"
      className="successModal"
      onClick={closeSuccessModal1}
    >
      <div className="modalContent">
        <div className="modalBody">
          <div className="title">
            Congratulation, You Have Catch {detail.name}
          </div>
          <div className="subTitle">Gotta Catch Them All</div>
          <div className="tryAgainButton" onClick={closeSuccessModal}>
            Close
          </div>
        </div>
      </div>
    </div>
  );

  return (
    <PokemonDetailDataWrapper display={display} successDisplay={successDisplay}>
      <div className="cardContainer">
        {modal}
        {successNoty}

        <div className="contentContainer">
          <div className="imageContainer">
            <div className="cardTitle">{detail.name}</div>

            <div className="imageFrame">
              <img src={detail.sprites.front_default} alt={detail.name}></img>
            </div>

            <div className="catchButton" onClick={getPokemon}>
              Catch It!
            </div>
          </div>

          <div className="textContainer">
            <div className="cardDetails">
              <div className="cardDetailContainer">
                Types
                <div className="cardTypes">
                  {detail.types.map((type, i) => {
                    return (
                      <div className="type" key={i}>
                        {type.type.name}
                      </div>
                    );
                  })}
                </div>
              </div>

              <div className="cardDetailContainer">
                Abilities
                <div className="cardAbilities">
                  {detail.abilities.map((ability, i) => {
                    return (
                      <div className="ability" key={i}>
                        {ability.ability.name}
                      </div>
                    );
                  })}
                </div>
              </div>

              <div className="cardDetailContainer">
                Moves:
                <div className="cardMoves">
                  {detail.moves.map((move, i) => {
                    return (
                      <div className="move" key={i}>
                        {move.move.name}
                      </div>
                    );
                  })}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </PokemonDetailDataWrapper>
  );
};

export default PokemonDetailData;
