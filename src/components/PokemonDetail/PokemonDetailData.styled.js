import styled from "@emotion/styled";

const PokemonDetailDataWrapper = styled.div`
    padding: 5% 5%;
    width: 80%;
    margin: auto;
    background-color: #f8f8ff;
    border-radius: 15px;

    .errorMessage{
        color:red;
    }

    .cardContainer{
        .contentContainer{
            display:flex;
        }

        .imageContainer{
            width: 50%;

            .imageFrame{
                position:relative;
                padding-top: 70%;
                width: 70%;
                margin: auto;
    
                img{
                    position: absolute;
                    top:50%;
                    left: 50%;
                    width: 90%;
                    transform:translate(-50%, -50%);
                    object-fit: cover;
                    border radius: 0 0 5px 5px;
                }
            }
    
            .cardTitle {
                text-transform: capitalize;
                text-align: center;
                font-size: 3.5vw;
                font-weight: 900;
                color: black;
            }
        }

        .textContainer {
            width: 50%;

            .cardDetails{
                border-left: 1px solid #AAAAAA;
                
                .cardTypes, .cardAbilities, .cardMoves{
                    display: flex;
                    justify-content: flex-start;
                    flex-wrap: wrap;
                    padding: 2% 0%;
                    font-weight: 800;
    
                    .type, .ability, .move{
                        padding: 2% 4%;
                        margin-right: 3%;
                        margin-bottom: 3%;
                        border-radius: 30px;
                        color:white;
                        font-weight: 700;
                        font-size: 1.2vw;
                    }

                    .type{
                        background-image: linear-gradient(90deg, #94F29E, #00DDB2);
                    }
    
                    .ability{
                        background-image: linear-gradient(135deg, #F194F2, #8900DD);
                    }
    
                    .move{
                        background-image: linear-gradient(135deg, #FFF687, #F6416C);
                    }
                }

                .cardDetailContainer{
                    padding: 0 2%;
                    text-align: left;
                }


            }
        }

        .catchButton {
            background-color: #F65416;
            color: white;
            font-weight: 700;
            width: 50%;
            margin: auto;
            display: block;
            border-radius: 10px;
            text-align: center;
            padding: 10px 0;
            cursor: pointer;
            transition: 0.5s ease;
            transition: transform 0.5s ease;
        
            :hover {
              transform: scale(1.03);
              box-shadow: 0 2px 2px 0 rgba(0, 0, 0, 0.2), 0 2px 2px 0 rgba(0, 0, 0, 0.2);
            }
        }
    }

    .successModal{
        display: ${(props) => props.successDisplay};
        position: fixed;
        z-index: 1;
        left: 0;
        top: 0;
        padding-top: 250px;
        width: 100%;
        height: 100%;
        overflow: auto;
        background-color: rgba(0, 0, 0, 0.4);
    }

    .modal {
        display: ${(props) => props.display};
        position: fixed;
        z-index: 1;
        left: 0;
        top: 0;
        padding-top: 150px;
        width: 100%;
        height: 100%;
        overflow: auto;
        background-color: rgba(0, 0, 0, 0.4);
    }

    .modalContent {
        position: relative;
        background-color: white;
        margin: auto;
        padding: 0;
        width: 50%;
        box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2),
          0 6px 20px 0 rgba(0, 0, 0, 0.19);
        -webkit-animation-name: animatetop;
        -webkit-animation-duration: 0.4s;
        animation-name: animatetop;
        animation-duration: 0.4s;
        border-radius: 15px;
      }
  
      .modalBody {
          padding: 5% 5%;;
          text-align: left;
      
          .title{
              text-transform: capitalize;
              font-size: 2.5vw;
              font-weight: 800;
              padding: 1% 0;
          }
      
          .subTitle{
              font-size: 1.5vw;
              padding: 1% 0 3% 0;
          }

          .modalBodyInput{
              margin-bottom: 3%;
              padding: 2% 2%;
              border: 1px solid #E0E0E0;
              border-radius: 15px;

              div{
                  color: #F86027;
                  padding-bottom: 1%;
              }

              input{
                  border: 0;
                  outline: 0;
                  background:transparent;
                  font-size: 1.5vw;
                  width: 100%;
              }
          }

          .tryAgainButton, .submitButton{
              width: 38%;
              text-align: center;
              padding: 10px 0;
              border: 1px solid #F86027;
              font-size: 1.5vw;
              font-weight: 800;
              border-radius: 10px;
              background-color: #F86027;
              color: white;
              margin-left: auto;
              cursor: pointer;
              transition: 0.5s;

              :hover{
                background-color: white;
                color: #F86027;
              }
          }
      }
    
    @-webkit-keyframes animatetop {
        from {top:-300px; opacity:0} 
        to {top:0; opacity:1}
    }

    @keyframes animatetop {
        from {top:-300px; opacity:0}
        to {top:0; opacity:1}
    }
        

    @media only screen and (max-width: 480px) {
        border-radius: 5px;

        .cardContainer{
            .contentContainer{
                display:block;
            }

            .imageContainer{
                width: 100%;

                .imageFrame{
                    padding-top: 80%;
                    width: 80%;
                }

                .cardTitle {
                    font-size: 9vw;
                }
            }

            .textContainer {
                width: 100%;

                .cardDetails{
                    border-left: 0;
                    border-top: 1px solid #AAAAAA;
                    padding-top: 7%;
                    
                    .cardTypes, .cardAbilities, .cardMoves{
                        justify-content: center;
                        padding: 5% 0;
                        font-weight: 100;
        
                        .type, .ability, .move{
                            padding: 3% 5%;
                            font-size: 3.5vw;
                        }
                    }

                    .cardDetailContainer{
                        text-align: center;
                        padding: 0%;
                    }


                }
            }

            .catchButton {
                width: 100%;
                margin-bottom: 7%;
            }
        }

        .successModal{
            display: ${(props) => props.successDisplay};
            position: fixed;
            z-index: 1;
            left: 0;
            top: 0;
            padding-top: 250px;
            width: 100%;
            height: 100%;
            overflow: auto;
            background-color: rgba(0, 0, 0, 0.4);
        }

        .modal {
            padding-top: 250px;
        }

        .modalContent {
            width: 80%;
        }
    
        .modalBody {
            padding: 5% 5%;;
        
            .title{
                font-size: 6vw;
                padding: 2% 0;
            }
        
            .subTitle{
                font-size: 3.5vw;
                padding: 1% 0 8% 0;
            }

            .modalBodyInput{
                margin-bottom: 5%;
                padding: 5% 6%;

                input{
                    font-size: 4vw;
                }
            }

            .tryAgainButton, .submitButton{
                width: 48%;
                font-size: 3vw;
            }
        }
    }
`;

export { PokemonDetailDataWrapper };
