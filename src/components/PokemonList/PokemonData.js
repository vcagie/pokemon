import React from "react";
import { Link } from "react-router-dom";
import { PokemonDataWrapper } from "./PokemonData.styled";
import pokeball from "../../images/pokeball1.png";

const PokemonData = ({ pokemon, ownedPokemon }) => {
  let ownedTotal = ownedPokemon.filter((Q) => Q.name === pokemon.name).length;

  let ownedDiv;
  if (ownedTotal === 0 ) {
    ownedDiv = (
        <div className="ownedPokemon">{ownedTotal}</div>
    );
  } else if(ownedTotal > 3){
    ownedDiv = (
      <div className="pokeballContainer">
        <PokeballList ownedTotal={3} />
        <div className="ownedPokemon">{ownedTotal - 3}+</div>
      </div>
    );
  }else {
    ownedDiv = (
      <div className="pokeballContainer">
        <PokeballList ownedTotal={ownedTotal} />
      </div>
    );
  }

  return (
    <PokemonDataWrapper>
      <Link
        to={{
          pathname: `/${pokemon.name}`,
        }}
      >
        <div className="cardContainer">
          <div className="imageFrame">
            <img src={pokemon.image} alt={pokemon.name}></img>
          </div>
          <div className="textContainer">
            <div className="cardTitle">{pokemon.name}</div>
            <div className="cardDetail">
              Owned: <div className="pokeballDetail">{ownedDiv}</div>
            </div>
          </div>
        </div>
      </Link>
    </PokemonDataWrapper>
  );
};

const PokeballList = ({ ownedTotal }) => {
  const pokeballs = [];
  for (let index = 0; index < ownedTotal; index++) {
    pokeballs.push(<img className="pokeball" src={pokeball} alt="" key={index}></img>);
  }

  return pokeballs;
};

export default PokemonData;
