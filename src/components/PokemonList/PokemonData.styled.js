import styled from "@emotion/styled";

const PokemonDataWrapper = styled.div`
  margin: 2% 0;
  width: 15%;
  background-image: linear-gradient(135deg, #ffdf91, #eaac7f);
  border-radius: 10px;
  transition: 0.7s;
  cursor: pointer;

  :hover {
    box-shadow: 0 5px 15px rgba(0, 0, 0, 0.3);
  }

  .cardContainer {
    margin: 5%;

    .imageFrame {
      width: 100%;
      padding-top: 100%;
      position: relative;
      background-color: white;
      border-radius: 5px;

      img {
        width: 80%;
        object-fit: cover;
        top: 50%;
        left: 50%;
        position: absolute;
        transform: translate(-50%, -50%);
      }
    }

    .textContainer {
      width: 100%;
      text-align: left;

      .cardTitle {
        text-transform: capitalize;
        font-size: 1.5vw;
        font-weight: 900;
        padding: 5% 0 5% 0;
        border-bottom: 1px solid white;
      }

      .cardDetail {
        padding: 5% 0;
        font-size: 1vw;
        position: relative;
        display: flex;

        .pokeballDetail {
          width: 100%;

        }
      }
    }
  }

  .ownedPokemon {
    margin-left: 5%;
    width: 25%;
    text-align: center;
    border-radius: 10px;
    background-color: #f65416;
    color: white;
  }

  .pokeballContainer {
    position: absolute;
    top: 50%;
    height: 70%;
    width: 70%;
    transform: translateY(-50%);
    display:flex;
    align-items:center;

    .pokeball {
      padding: 0 1%;
      height: 100%;
    }
  }

  @media only screen and (max-width: 480px) {
    width: 47%;

    .cardContainer {
      .textContainer {
        .cardTitle {
          font-size: 5vw;
          padding: 10% 0 5% 0;
          border-bottom: 2px solid white;
        }

        .cardDetail {
          font-size: 3vw;
        }
      }
    }
  }
`;

export { PokemonDataWrapper };
