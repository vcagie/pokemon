import styled from "@emotion/styled";

const MyPokemonDataWrapper = styled.div`
  width: 15%;
  border-radius: 10px;
  background-image: linear-gradient(135deg, #ffdf91, #eaac7f);
  margin: 2% 0;

  .cardContainer {
    margin: 5%;

    .imageFrame {
      position: relative;
      width: 100%;
      padding-top: 100%;
      border-radius: 5px;
      background-color: white;

      img {
        width: 80%;
        position: absolute;
        top: 45%;
        left: 50%;
        transform: translate(-50%, -50%);
        object-fit: cover;
      }

      .cardTitle {
        position: absolute;
        bottom: 5px;
        left: 5px;
        padding: 2% 4%;
        text-transform: capitalize;
        font-weight: 900;
        color: #606060;
        background-color: #c5c5c5;
        font-size: 1vw;
        border-radius: 3px;
      }
    }

    .textContainer {
      width: 100%;
      text-align: left;
      margin: 5% 0;

      .cardNickname {
        font-size: 1vw;

        .nickname {
          font-size: 1.5vw;
          color: black;
          font-weight: 900;
        }
      }
    }
  }

  .releaseButton {
    text-align: center;
    width: 100%;
    padding: 3% 0;
    border-radius: 10px;
    background-color: white;
    color: #f86027;
    font-weight: 700;
    font-size: 1vw;
    border: 1px solid #f86027;
    margin: auto;
    cursor: pointer;
    transition: 0.5s;

    :hover,
    :active {
      color: white;
      background-color: #f86027;
    }
  }

  @media only screen and (max-width: 480px) {
    width: 47%;

    .cardContainer {
      .imageFrame {
        .cardTitle {
          font-size: 3vw;
        }
      }

      .textContainer {
        .cardNickname {
          font-size: 3vw;
  
          .nickname {
            font-size: 5vw;
          }
        }
      }
    }

    .releaseButton {
      font-size: 3.5vw;
    }
  }
`;

export { MyPokemonDataWrapper };
