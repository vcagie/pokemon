import React from "react";
import { MyPokemonDataWrapper } from "./MyPokemonCard.style";

const MyPokemonCard = ({ data, getPokemonInfo, showModal }) => {
  const openModal = (nickname, name) =>{
    showModal("block");
    getPokemonInfo(nickname,name)
  }

  return (
    <MyPokemonDataWrapper>
      <div className="cardContainer">
        <div className="imageFrame">
          <img src={data.sprites.front_default} alt={data.name}></img>
          <div className="cardTitle">{data.name}</div>
        </div>

        <div className="textContainer">
          <div className="cardNickname">
            Nickname:
            <div className="nickname">{data.nickname}</div>
          </div>
        </div>

        <div
          className="releaseButton"
          onClick={() => {
              openModal(data.nickname, data.name);
          }}
        >
          Release Pokemon
        </div>
      </div>
    </MyPokemonDataWrapper>
  );
};

export default MyPokemonCard;
