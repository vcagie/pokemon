import React from 'react';
import {LoadingWrapper} from './LoadingPage.style';

const LoadingPage = () =>{
    return (
      <LoadingWrapper>
        <div className="loader"></div>
      </LoadingWrapper>
    );
}

export default LoadingPage;