import React from 'react';
import {ErrorWrapper} from './errorPage.style';
import errorImg from '../../images/error.png';

const ErrorPage = () =>{
    return (
      <ErrorWrapper>
        <img src={errorImg} alt="error"></img>
        <div className="errorText">
            PAGE NOT FOUND!
        </div>
      </ErrorWrapper>
    );
}

export default ErrorPage;