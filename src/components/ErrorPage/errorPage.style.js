import styled from '@emotion/styled';

const ErrorWrapper = styled.div`
    img{
        display:block;
        width: 40%;
        margin:auto;
    }
    .errorText {
        padding: 10px 0;
        text-align: center;
        font-size: 25px;
        font-weight: 700;
    }
`

export {ErrorWrapper}